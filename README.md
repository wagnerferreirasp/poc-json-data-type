# poc-json-data-type

Proof of concept: Json Data types with Spring and MySQL 8


### Create Local Mysql 8 with docker
 ```sh
docker run -it -p 3306:3306 -e MYSQL_ROOT_PASSWORD=senha123 -e MYSQL_DATABASE=testedb mysql:8.0.20
```

## Run project
```sh
mvn spring-boot:run
```

## Endpoints

### Create author
Endpoint: ```POST /author```

Request Payload: 
```json
{
    "book": {
        "a": 7,
        "b": 11
    }
}
```

### Get all authors by Book.b

Endpoint: ```GET /author?b={int}```

Lists all authors filtering by Book.b property

