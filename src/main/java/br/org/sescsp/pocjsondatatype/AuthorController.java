package br.org.sescsp.pocjsondatatype;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/author")
public class AuthorController {
    @Autowired
    AuthorRepository repository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createAuthor(@RequestBody AuthorDTO authorDTO) {
        Author author = new Author(null, authorDTO.getBook());
        repository.save(author);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Author> findAllByB(@RequestParam String b) {
        return repository.findAllByBookB(b);
    }
}
